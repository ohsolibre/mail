#define _GNU_SOURCE
#include <err.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <sys/types.h>

#define IS_ERR(x) ((x)[0] != '+' || (x)[1] != 'O' || (x)[2] != 'K')
#include "lib.c"

static void pop3_retr(BIO *bio, int num)
{
	swrite(bio, "RETR %i\r\n", num);
	for (;;) {
		if (write(1, sbuf, slen) != slen)
			err(1, "write");
		if (memmem(sbuf, slen, "\r\n.\r\n", 5))
			break;
		if ((slen = BIO_read(bio, sbuf, sizeof(sbuf))) <= 0)
			errx(1, "BIO_read: %s\n", SSL_ERR);
	}
}

int main(int argc, char **argv)
{
	BIO *bio;
	int i, total, delete = 0;
	char *pwd, *user, *host;

	if (argc >= 2 && !strcmp(argv[1], "-d")) {
		argv++;
		delete = 1;
	}

	if (!(user = argv[1]) || !(host = argv[2]))
		errx(1, "usage: %s [-d] <user> <host>", argv[0]);
	pwd = pwdread();

	bio = sopen(host, "995");
	sread(bio);

	swrite(bio, "USER %s\r\n", user);
	swrite(bio, "PASS %s\r\n", pwd);
	swrite(bio, "STAT\r\n");

	total = sbuf[3] == ' ' ? atoi(&sbuf[4]) : 0;
	for (i = 1; i <= total; i++) {
		pop3_retr(bio, i);
		if (delete)
			swrite(bio, "DELE %i\r\n", i);
	}

	swrite(bio, "QUIT\r\n");
	BIO_free_all(bio);
}
