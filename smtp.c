#include <err.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <getopt.h>
#include <sys/stat.h>
#include <sys/types.h>

#define IS_ERR(x) ((x)[0] != '2' && (x)[0] != '3')
#include "lib.c"

static char *base64(char *s)
{
	size_t i, n = strlen(s);
	char *b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
	char *buf = malloc(n * 4 / 3 + 4);

	for (i = 0; n >= 3; n -= 3, s += 3) {
		buf[i++] = b64[s[0] >> 2];
		buf[i++] = b64[((s[0] & 0x03) << 4) + (s[1] >> 4)];
		buf[i++] = b64[((s[1] & 0x0f) << 2) + (s[2] >> 6)];
		buf[i++] = b64[s[2] & 0x3f];
	}

	if (n > 0) {
		buf[i++] = b64[s[0] >> 2];
		if (n == 1) {
			buf[i++] = b64[(s[0] & 0x03) << 4];
			buf[i++] = '=';
		} else {
			buf[i++] = b64[((s[0] & 0x03) << 4) + (s[1] >> 4)];
			buf[i++] = b64[(s[1] & 0x0f) << 2];
		}
		buf[i++] = '=';
	}
	return buf[i] = '\0', buf;
}

static char *file_read(char *name)
{
	int fd;
	char *s;
	struct stat st;

	if ((fd = open(name, O_RDONLY)) < 0)
		err(1, "open %s\n", name);
	if (fstat(fd, &st) < 0)
		err(1, "fstat");
	s = malloc(st.st_size + 1);
	if (read(fd, s, st.st_size) != st.st_size)
		err(1, "read");
	s[st.st_size] = '\0';
	close(fd);
	return s;
}

int main(int argc, char **argv)
{
	BIO *bio;
	int opt;
	char *pwd, *user, *host, *file, *rcpt, *subject;

	file = rcpt = subject = NULL;
	while ((opt = getopt(argc, argv, "s:r:f:")) != -1) {
		switch (opt) {
		case 's':
			subject = optarg;
			break;
		case 'r':
			rcpt = optarg;
			break;
		case 'f':
			file = optarg;
			break;
		}
	}

	if (!file || !subject || !rcpt || !(user = argv[optind]) || !(host = argv[optind+1]))
		errx(1, "usage: %s -f <file> -s <subject> -r <recipient> <user> <host>", argv[0]);

	pwd = pwdread();
	file = file_read(file);
	bio = sopen(host, "465");
	sread(bio);

	swrite(bio, "EHLO %s\r\n", host);
	swrite(bio, "AUTH LOGIN\r\n");
	swrite(bio, "%s\n", base64(user));
	swrite(bio, "%s\n", base64(pwd));
	swrite(bio, "MAIL FROM:<%s@%s>\r\n", user, host);
	swrite(bio, "RCPT TO:<%s>\r\n", rcpt);
	swrite(bio, "DATA\r\n", user, host);
	swrite(bio, "Date: Wed, 1 Jan 2020 10:00:00 +0000\n"
		    "To: <%s>\n"
		    "From: <%s@%s>\n"
		    "Subject: %s\n"
		    "%s\r\n.\r\n", rcpt, user, host, subject, file);

	swrite(bio, "QUIT\r\n");
	BIO_free_all(bio);
}
