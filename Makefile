CC = cc
CFLAGS = -Wall -Wextra -Os
LDFLAGS = -static -lssl -lcrypto

all: smtp pop3
%: %.c lib.c
	$(CC) -o $@ $< $(CFLAGS) $(LDFLAGS)
clean:
	$(RM) smtp pop3
