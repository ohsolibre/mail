#include <stdarg.h>
#include <termios.h>
#include <openssl/ssl.h>
#include <openssl/err.h>

#define SSL_ERR (ERR_reason_error_string(ERR_get_error()))

static char sbuf[4096];
static int slen;

static char *pwdread(void)
{
	static char buf[64];
	long len;
	struct termios newtios, oldtios;

	tcgetattr(0, &oldtios);
	newtios = oldtios;
	newtios.c_lflag &= ~ECHO;

	tcsetattr(0, TCSAFLUSH, &newtios);
	write(2, "Password: ", 10);
	len = read(0, buf, sizeof(buf) - 1);
	write(2, "\n", 1);
	tcsetattr(0, TCSAFLUSH, &oldtios);

	if (len > 0) {
		if (buf[len - 1] == '\n')
			--len;
		buf[len] = '\0';
	} else
		exit(1);
	return buf;
}

static BIO *sopen(char *host, char *port)
{
	BIO *bio;
	SSL *ssl;
	SSL_CTX *ctx;

	if ((ctx = SSL_CTX_new(TLS_client_method())) == NULL)
		goto fail;
	if (SSL_CTX_set_default_verify_paths(ctx) == 0)
		goto fail;
	SSL_CTX_set_verify(ctx, SSL_VERIFY_PEER, NULL);

	if ((bio = BIO_new_ssl_connect(ctx)) == NULL)
		goto fail;
	BIO_get_ssl(bio, &ssl);
	SSL_set_tlsext_host_name(ssl, host);
	if (!SSL_set1_host(ssl, host))
		goto fail;

	BIO_set_conn_hostname(bio, host);
	BIO_set_conn_port(bio, port);
	if (BIO_do_connect(bio) <= 0)
		goto fail;

	fprintf(stderr, "connected to '%s:%s'\n", host, port);
	return bio;
fail:
	errx(1, "cannot connect to '%s:%s': %s", host, port, SSL_ERR);
}

static void sread(BIO *bio)
{
	if ((slen = BIO_read(bio, sbuf, sizeof(sbuf))) <= 0)
		errx(1, "BIO_read: %s", SSL_ERR);
	if (IS_ERR(sbuf))
		errx(1, "%.*s", slen, sbuf);
}

static void swrite(BIO *bio, char *s, ...)
{
	va_list ap;
	char *buf;

	va_start(ap, s);
	if (vasprintf(&buf, s, ap) < 0)
		errx(1, "vasprintf");
	if (BIO_puts(bio, buf) <= 0)
		errx(1, "BIO_puts: %s", SSL_ERR);
	free(buf);
	va_end(ap);
	sread(bio);
}
